﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json.Serialization;
using Microsoft.Data.SqlClient;
using System.Threading;
using System.Linq;

namespace TaskA2
{
    public class Content
    {
        [JsonPropertyName("sellerName")]
        public string sellerName { get; set; }
        [JsonPropertyName("sellerInn")]
        public string sellerInn { get; set; }
        [JsonPropertyName("buyerName")]
        public string buyerName { get; set; }
        [JsonPropertyName("buyerInn")]
        public string buyerInn { get; set; }
        [JsonPropertyName("woodVolumeBuyer")]
        public string woodVolumeBuyer { get; set; }
        [JsonPropertyName("woodVolumeSeller")]
        public string woodVolumeSeller { get; set; }

        [JsonPropertyName("dealDate")]
        public string dealDate { get; set; }
        [JsonPropertyName("dealNumber")]
        public string dealNumber { get; set; }
        [JsonPropertyName("__typename")]
        public string typename { get; set; }

    }

    public class Data
    {
        [JsonPropertyName("searchReportWoodDeal")]
        public SearchReportWoodDeal searchReportWoodDeal { get; set; }
    }

    public class Root
    {
        [JsonPropertyName("data")]
        public Data data { get; set; }
    }

    public class SearchReportWoodDeal
    {
        [JsonPropertyName("content")]
        public List<Content> content { get; set; }
        [JsonPropertyName("total")]
        public int total { get; set; }
        public string __typename { get; set; }
    }

    class Program
    {
        
        static string connectionString = "Server=(localdb)\\mssqllocaldb;Database=data;Trusted_Connection=True";

        public static void Main()
        {
            while (true)
            {
                UpdateAndAddingContentFromLesegais();               
                Thread.Sleep(600000);
            }

        }

        public static void UpdateAndAddingContentFromLesegais()
        {
            var contentLen = 3000;

            for (int i = 0; ; i++)
            {

                var parsingData = ParseFromLesegais(contentLen, i);
                List<Content> listContent = parsingData.content;
                var total = parsingData.total;
                if (3000 * i - total > 3001)
                {
                    //20:13:21 - 20:17:57 - время одного обхода всех записей
                    return;
                }
                else
                {
                    foreach (var content in listContent)
                    {
                        AddToDB(content);
                    }
                }
            }
        }

        public static SearchReportWoodDeal ParseFromLesegais(int size, int page)
        {
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Post, "https://www.lesegais.ru/open-area/graphql");
            var content = new StringContent("{\r\n    \"query\": \"query SearchReportWoodDeal($size: Int!, $number: Int!, $filter: Filter, $orders: [Order!]) {\\n  searchReportWoodDeal(filter: $filter, pageable: {number: $number, size: $size}, orders: $orders) {\\n total\\n   content {\\n      sellerName\\n      sellerInn\\n      buyerName\\n      buyerInn\\n      woodVolumeBuyer\\n      woodVolumeSeller\\n      dealDate\\n      dealNumber\\n      __typename\\n    }\\n    __typename\\n  }\\n}\\n\",\r\n    \"variables\": {\r\n        \"size\": "+size+",\r\n        \"number\": "+page+",\r\n        \"filter\": null,\r\n        \"orders\": null\r\n    },\r\n    \"operationName\": \"SearchReportWoodDeal\"\r\n}", null, "application/json");

            request.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36");
            request.Content = content;
            
            var response = client.Send(request);
            
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            var fromJsonResult = JsonConvert.DeserializeObject<Root>(result);

            return fromJsonResult.data.searchReportWoodDeal;
        }

        public static void AddToDB(Content content)
        {
            var buyerName = !String.IsNullOrEmpty(content.buyerName) ? content.buyerName.Replace('\'', ' ') : content.buyerName;
            var sellerName = !String.IsNullOrEmpty(content.sellerName) ? content.sellerName.Replace('\'', ' ') : content.sellerName;
            var buyerInn = !String.IsNullOrEmpty(content.buyerInn) ? content.buyerInn.Replace('\'', ' ') : content.buyerInn;
            var sellerInn = !String.IsNullOrEmpty(content.sellerInn) ? content.sellerInn.Replace('\'', ' ') : content.sellerInn;

            var sqlExpression = $"INSERT INTO ReportWoodDeal (sellerName, sellerInn, buyerName, buyerInn, woodVolumeBuyer, woodVolumeSeller, dealDate, dealNumber, typename) " +
                $"VALUES (N'{sellerName}'," +
                $"N'{sellerInn}'," +
                $"N'{buyerName}'," +
                $"N'{buyerInn}'," +
                $"'{content.woodVolumeBuyer}'," +
                $"'{content.woodVolumeSeller}'," +
                $"'{content.dealDate}'," +
                $"'{content.dealNumber}', " +
                $"'{content.typename}')";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                try
                {
                    int number = command.ExecuteNonQuery();
                }
                catch(SqlException ex)
                {
                    Console.WriteLine("Такие данные уже есть");
                    if (isNeededUpdate(content))
                    {
                        Console.WriteLine("Нужно обновить");
                        UpdateContent(content);
                    }
                    else Console.WriteLine("Обновлять не нужно");
                }

               Console.WriteLine("Данные добавлены или обновлены");
                
            }
        }

        public static bool isNeededUpdate(Content content)
        {
            var buyerName = !String.IsNullOrEmpty(content.buyerName) ? content.buyerName.Replace('\'', ' ') : content.buyerName;
            var sellerName = !String.IsNullOrEmpty(content.sellerName) ? content.sellerName.Replace('\'', ' ') : content.sellerName;
            var buyerInn = !String.IsNullOrEmpty(content.buyerInn) ? content.buyerInn.Replace('\'', ' ') : content.buyerInn;
            var sellerInn = !String.IsNullOrEmpty(content.sellerInn) ? content.sellerInn.Replace('\'', ' ') : content.sellerInn;

            var sqlExpression = $"SELECT COUNT(dealNumber) FROM ReportWoodDeal WHERE dealNumber = '{content.dealNumber}' " +
                $" AND dealDate = '{content.dealDate}' " +
                $" AND sellerName = N'{sellerName}' " +
                $" AND buyerName = N'{buyerName}' " +
                $" AND sellerInn = N'{sellerInn}' " +
                $" AND buyerInn = N'{buyerInn}' " +
                $" AND WoodVolumeBuyer = '{content.woodVolumeBuyer}' " +
                $" AND WoodVolumeSeller = '{content.woodVolumeSeller}' ";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                var number = command.ExecuteReader();
                while(number.Read())
                {
                    object id = number.GetValue(0);
                    if (id.ToString() == "0") return true;
                    else return false;
                }


                return true;
            }

        }

        public static void UpdateContent(Content content)
        {
            var buyerName = !String.IsNullOrEmpty(content.buyerName) ? content.buyerName.Replace('\'', ' ') : content.buyerName;
            var sellerName = !String.IsNullOrEmpty(content.sellerName) ? content.sellerName.Replace('\'', ' ') : content.sellerName;
            var buyerInn = !String.IsNullOrEmpty(content.buyerInn) ? content.buyerInn.Replace('\'', ' ') : content.buyerInn;
            var sellerInn = !String.IsNullOrEmpty(content.sellerInn) ? content.sellerInn.Replace('\'', ' ') : content.sellerInn;

            var sqlExpression = $"UPDATE ReportWoodDeal SET sellerName=N'{sellerName}', " +
                $"sellerInn=N'{sellerInn}', " +
                $"buyerName=N'{buyerName}', " +
                $"buyerInn=N'{buyerInn}', " +
                $"woodVolumeBuyer='{content.woodVolumeBuyer}', " +
                $"woodVolumeSeller='{content.woodVolumeSeller}' " +
                $"WHERE dealNumber='{content.dealNumber}' AND dealDate='{content.dealDate}' ";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                var number = command.ExecuteNonQuery();
                Console.WriteLine("Данные изменены");
            }
        }

    }

    
}
